import 'dart:math';

import 'package:fluffychat/config/app_config.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:vrouter/vrouter.dart';

import 'package:fluffychat/pages/new_private_chat/new_private_chat.dart';
import 'package:fluffychat/utils/platform_infos.dart';
import 'package:fluffychat/widgets/layouts/max_width_body.dart';

import '../../widgets/avatar.dart';
import '../../widgets/matrix.dart';

class NewPrivateChatView extends StatelessWidget {
  final NewPrivateChatController controller;

  const NewPrivateChatView(this.controller, {Key? key}) : super(key: key);

  static const double _qrCodePadding = 8;

  @override
  Widget build(BuildContext context) {
    if (controller.qrData == '') controller.generateQrData();
    final qrCodeSize =
        min(MediaQuery.of(context).size.width - 16, 256).toDouble();
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        title: Text(L10n.of(context)!.newChat),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        actions: [
          if (AppConfig.isTeacher == true)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                onPressed: () => VRouter.of(context).to('/newgroup'),
                child: Text(
                  L10n.of(context)!.createNewGroup,
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
              ),
            )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: MaxWidthBody(
              withScrolling: true,
              child: Container(
                margin: const EdgeInsets.all(_qrCodePadding),
                alignment: Alignment.center,
                padding: const EdgeInsets.all(_qrCodePadding * 2),
                child: Material(
                  borderRadius: BorderRadius.circular(12),
                  elevation: 10,
                  color: Colors.white,
                  shadowColor: Theme.of(context).appBarTheme.shadowColor,
                  clipBehavior: Clip.hardEdge,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // const SizedBox(
                      //   height: 20,
                      // ),
                      FutureBuilder<Profile>(
                        future: controller.profileFuture(),
                        builder: (context, snapshot) {
                          final profile = snapshot.data;
                          final mxid = Matrix.of(context).client.userID ??
                              L10n.of(context)!.user;
                          final displayname =
                              profile?.displayName ?? mxid.localpart ?? mxid;
                          return Center(
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 16.0,
                                    right: 16.0,
                                    top: 8,
                                    bottom: 8,
                                  ),
                                  child: Stack(
                                    children: [
                                      Material(
                                        elevation: Theme.of(context)
                                                .appBarTheme
                                                .scrolledUnderElevation ??
                                            4,
                                        shadowColor: Theme.of(context)
                                            .appBarTheme
                                            .shadowColor,
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                            color:
                                                Theme.of(context).dividerColor,
                                          ),
                                          borderRadius: BorderRadius.circular(
                                            Avatar.defaultSize * 2.5,
                                          ),
                                        ),
                                        child: Avatar(
                                          mxContent: profile?.avatarUrl,
                                          name: displayname,
                                          size: Avatar.defaultSize,
                                          fontSize: 18 * 2.5,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        displayname,
                                        style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        //  style: const TextStyle(fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      // Text(

                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: QrImage(
                          data: controller.qrData,
                          version: QrVersions.auto,
                          size: qrCodeSize,
                        ),
                      ),
                      if (AppConfig.isTeacher)
                        TextButton.icon(
                          style: TextButton.styleFrom(
                            fixedSize: Size.fromWidth(
                              qrCodeSize - (2 * _qrCodePadding),
                            ),
                            foregroundColor: Colors.black,
                          ),
                          icon: Icon(Icons.adaptive.share_outlined),
                          label: Text(L10n.of(context)!.shareYourInviteLink),
                          onPressed: controller.inviteAction,
                        ),
                      const SizedBox(height: 8),
                      if (PlatformInfos.isMobile) ...[
                        OutlinedButton.icon(
                          style: OutlinedButton.styleFrom(
                            textStyle: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                            backgroundColor:
                                Theme.of(context).colorScheme.primary,
                            foregroundColor:
                                Theme.of(context).colorScheme.onPrimary,
                            fixedSize: Size(qrCodeSize + 20, 60),
                          ),
                          icon: const Icon(Icons.qr_code_scanner_outlined),
                          label: const Text('EINLADUNG SCANNEN'),
                          onPressed: controller.openScannerAction,
                        ),
                        const SizedBox(height: 20),
                      ],
                    ],
                  ),
                ),
              ),
            ),
          ),
          const Center(
            child: Text('oder suche nach einem Kontakt der Hermannschule'),
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.all(12),
            child: FloatingActionButton.extended(
              backgroundColor: Theme.of(context).colorScheme.primary,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              onPressed: () => controller.openContactsRoom(),
              label: const Text(
                'KONTAKTE HERMANNSCHULE',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              icon: const Icon(Icons.groups_2_rounded),
            ),
          ),
          if (AppConfig.isTeacher == true)
            MaxWidthBody(
              withScrolling: false,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Form(
                  key: controller.formKey,
                  child: TextFormField(
                    controller: controller.controller,
                    autocorrect: false,
                    textInputAction: TextInputAction.go,
                    focusNode: controller.textFieldFocus,
                    onFieldSubmitted: controller.submitAction,
                    validator: controller.validateForm,
                    inputFormatters: controller.removeMatrixToFormatters,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 6,
                      ),
                      labelText: L10n.of(context)!.enterInviteLinkOrMatrixId,
                      hintText: '@username',
                      prefixText: NewPrivateChatController.prefixNoProtocol,
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.send_outlined),
                        onPressed: controller.submitAction,
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
//import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';

import 'package:fluffychat/pages/new_private_chat/new_private_chat_view.dart';
//import 'package:fluffychat/pages/new_private_chat/qr_scanner_modal.dart';
//import 'package:fluffychat/utils/adaptive_bottom_sheet.dart';
import 'package:fluffychat/utils/fluffy_share.dart';
import 'package:fluffychat/utils/platform_infos.dart';
import 'package:fluffychat/utils/url_launcher.dart';
import 'package:fluffychat/utils/qr_encryption.dart';

import 'package:fluffychat/widgets/matrix.dart';
import 'package:barcode_scan2/barcode_scan2.dart' show BarcodeScanner;
//import 'package:vrouter/vrouter.dart';

class NewPrivateChat extends StatefulWidget {
  const NewPrivateChat({Key? key}) : super(key: key);

  @override
  NewPrivateChatController createState() => NewPrivateChatController();
}

class NewPrivateChatController extends State<NewPrivateChat> {
  final TextEditingController controller = TextEditingController();
  final FocusNode textFieldFocus = FocusNode();
  final formKey = GlobalKey<FormState>();

  String schoolDirectoryRoomId = '!gqnnuXGiaupQKSwCWB:hermannschule.de';
  bool loading = false;
  String qrData = '';
  Future<Profile>? profileFuture() {
    final client = Matrix.of(context).client;
    return client.getProfileFromUserId(
      client.userID!,
      cache: true,
      getFromRooms: true,
    );
  }

  // remove leading matrix.to from text field in order to simplify pasting
  final List<TextInputFormatter> removeMatrixToFormatters = [
    FilteringTextInputFormatter.deny(NewPrivateChatController.prefix),
    FilteringTextInputFormatter.deny(NewPrivateChatController.prefixNoProtocol),
  ];

  static const Set<String> supportedSigils = {'@', '!', '#'};

  static const String prefix = 'https://matrix.to/#/';
  static const String prefixNoProtocol = 'matrix.to/#/';

  void submitAction([_]) async {
    controller.text = controller.text.trim();
    if (!formKey.currentState!.validate()) return;
    UrlLauncher(context, '$prefix${controller.text}').openMatrixToUrl();
  }

  void openContactsRoom([_]) async {
    UrlLauncher(context, 'https://matrix.to/#/#kontakte:hermannschule.de')
        .openMatrixToUrl();
  }

  generateQrData() async {
    final userID = Matrix.of(context).client.userID;
    final inviteLink = 'https://matrix.to/#/$userID';

    final String inviteLinkEncrypted = await QrEncrypter().encrypt(inviteLink);
    setState(() {
      qrData = inviteLinkEncrypted;
    });
    return (inviteLinkEncrypted);
  }

  String? validateForm(String? value) {
    if (value!.isEmpty) {
      return L10n.of(context)!.pleaseEnterAMatrixIdentifier;
    }
    if (!controller.text.isValidMatrixId ||
        !supportedSigils.contains(controller.text.sigil)) {
      return L10n.of(context)!.makeSureTheIdentifierIsValid;
    }
    if (controller.text == Matrix.of(context).client.userID) {
      return L10n.of(context)!.youCannotInviteYourself;
    }
    return null;
  }

  void inviteAction() => FluffyShare.share(
        'https://matrix.to/#/${Matrix.of(context).client.userID}',
        context,
      );

  void openScannerAction() async {
    if (PlatformInfos.isAndroid) {
      final info = await DeviceInfoPlugin().androidInfo;
      if (info.version.sdkInt < 21) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              L10n.of(context)!.unsupportedAndroidVersionLong,
            ),
          ),
        );
        return;
      }
    }
    final qrScanResult = await BarcodeScanner.scan();
    useInvitation(qrScanResult.rawContent);
  }

  void useInvitation(encryptedScan) async {
    final decryptedInvitation = await QrEncrypter().decrypt(encryptedScan);

    UrlLauncher(context, decryptedInvitation).openMatrixToUrl();
  }

  @override
  Widget build(BuildContext context) => NewPrivateChatView(this);
}

import 'package:fluffychat/config/app_config.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';

// import 'package:fluffychat/utils/fluffy_share.dart';
import 'package:fluffychat/widgets/avatar.dart';
import '../../utils/fluffy_share.dart';
import '../../utils/matrix_sdk_extensions/presence_extension.dart';
import '../../widgets/matrix.dart';
import 'user_bottom_sheet.dart';

class UserBottomSheetView extends StatelessWidget {
  final UserBottomSheetController controller;

  const UserBottomSheetView(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = controller.widget.user;
    final client = Matrix.of(context).client;
    //final mxid = Matrix.of(context).client.userID ?? L10n.of(context)!.user;

    final presence = client.presences[user.id];
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: CloseButton(
            onPressed: Navigator.of(context, rootNavigator: false).pop,
          ),
          title: Text(user.calcDisplayname()),
        ),
        body: ListView(
          children: [
            Center(
              child: Avatar(
                mxContent: user.avatarUrl,
                name: user.calcDisplayname(),
                size: Avatar.defaultSize * 3,
                fontSize: 36,
              ),
              // Expanded(
              //   child: ListTile(
              //     contentPadding: const EdgeInsets.only(right: 16.0),
              //     title: Text(user.id),
              //     subtitle: presence == null
              //         ? null
              //         : Text(presence.getLocalizedLastActiveAgo(context)),
              //     trailing: IconButton(
              //       icon: Icon(Icons.adaptive.share),
              //       onPressed: () => FluffyShare.share(
              //         user.id,
              //         context,
              //       ),
              //     ),
              //   ),
              // ),
            ),
            if (AppConfig.isTeacher == true)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextButton.icon(
                  onPressed: () => FluffyShare.share(user.id, context),
                  icon: const Icon(
                    Icons.copy_outlined,
                    size: 14,
                  ),
                  style: TextButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.secondary,
                  ),
                  label: Text(
                    user.id,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    //    style: const TextStyle(fontSize: 12),
                  ),
                ),
              ),
            if (presence != null)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(presence.getLocalizedLastActiveAgo(context)),
                ),
              ),
            if (controller.widget.onMention != null &&
                AppConfig.isTeacher == true)
              ListTile(
                leading: const Icon(Icons.alternate_email_outlined),
                title: Text(L10n.of(context)!.mention),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.mention),
              ),
            if (user.canChangePowerLevel)
              ListTile(
                title: Text(L10n.of(context)!.setPermissionsLevel),
                leading: const Icon(Icons.edit_attributes_outlined),
                onTap: () => controller
                    .participantAction(UserBottomSheetAction.permission),
              ),
            if (user.canKick)
              ListTile(
                title: Text(L10n.of(context)!.kickFromChat),
                leading: const Icon(Icons.exit_to_app_outlined),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.kick),
              ),
            if (user.canBan && user.membership != Membership.ban)
              ListTile(
                title: Text(L10n.of(context)!.banFromChat),
                leading: const Icon(Icons.warning_sharp),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.ban),
              )
            else if (user.canBan && user.membership == Membership.ban)
              ListTile(
                title: Text(L10n.of(context)!.unbanFromChat),
                leading: const Icon(Icons.warning_outlined),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.unban),
              ),
            if (user.id != client.userID &&
                !client.ignoredUsers.contains(user.id))
              ListTile(
                textColor: Theme.of(context).colorScheme.onErrorContainer,
                iconColor: Theme.of(context).colorScheme.onErrorContainer,
                title: Text(L10n.of(context)!.ignore),
                leading: const Icon(Icons.block),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.ignore),
              ),
            if (user.id != client.userID)
              ListTile(
                textColor: Theme.of(context).colorScheme.error,
                iconColor: Theme.of(context).colorScheme.error,
                title: Text(L10n.of(context)!.reportUser),
                leading: const Icon(Icons.shield_outlined),
                onTap: () =>
                    controller.participantAction(UserBottomSheetAction.report),
              ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(12),
              child: FloatingActionButton.extended(
                backgroundColor: Theme.of(context).colorScheme.primary,
                foregroundColor: Theme.of(context).colorScheme.onPrimary,
                onPressed: () =>
                    controller.participantAction(UserBottomSheetAction.message),
                label: Text(
                  L10n.of(context)!.newChat.toUpperCase(),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                icon: const Icon(Icons.send_outlined),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

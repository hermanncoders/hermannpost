import 'dart:async';
import 'package:barcode_scan2/barcode_scan2.dart' show BarcodeScanner;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:matrix/matrix.dart';

import 'package:fluffychat/config/app_config.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'package:fluffychat/utils/localized_exception_extension.dart';
import 'package:fluffychat/utils/qr_encryption.dart';
import 'package:fluffychat/widgets/matrix.dart';
import '../../config/setting_keys.dart';
import '../../utils/platform_infos.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class QrLoginScreen extends StatefulWidget {
  const QrLoginScreen({Key? key}) : super(key: key);

  @override
  QrLoginState createState() => QrLoginState();
}

class QrLoginState extends State<QrLoginScreen> {
  String? usernameError;
  String? passwordError;
  bool loading = false;
  late String base64encryptedLoginID;
  late String newloginID;
  late String myloginID;
  late String matrixID;
  late String username;
  late String password;
  late String
      passwordCode; // TO DO: implement store password code to be able to change the PIN later
  late String homeserver;
  late String passwordPin;

  String loginScanMessage =
      'Scanne den QR-Schlüssel, den du von deiner Schule bekommen hast!';
  String loginScanImage = 'assets/hermannkey.png';

  void credentials(decryptedString) async {
    myloginID = decryptedString;

    if (myloginID[0] == '1') {
      await Matrix.of(context).store.setItemBool(SettingKeys.isTeacher, true);
      setState(() {});
      AppConfig.isTeacher = true;
      myloginID = myloginID.substring(1);

      await Matrix.of(context).store.setItemBool(SettingKeys.isTeacher, true);
      if (kDebugMode) {
        print('Teacher mode ON');
        showTopSnackBar(
          Overlay.of(context),
          const CustomSnackBar.success(
            backgroundColor: Colors.green,
            message: 'Teacher mode is ON',
          ),
        );
      }
    } else {
      AppConfig.isTeacher = false;
      await Matrix.of(context).store.setItemBool(SettingKeys.isTeacher, false);
      if (kDebugMode) {
        showTopSnackBar(
          animationDuration: const Duration(milliseconds: 1600),
          displayDuration: const Duration(
            milliseconds: 80,
          ),
          snackBarPosition: SnackBarPosition.bottom,
          dismissDirection: [DismissDirection.horizontal],
          Overlay.of(context),
          const CustomSnackBar.error(
            textStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.white,
            ),
            message: 'Teachermode is OFF',
            icon: Icon(
              Icons.school,
              color: Color(0xffff5252),
            ),
          ),
        );
      }
    }

    matrixID = myloginID.split('*').first;
    password = myloginID.split('*').last;
    password = password.trim();
    password = password.substring(0, 8);
    homeserver = matrixID.split(':').last;
    username = matrixID.split(':').first;

    loginScanImage = 'assets/scansuccess.png';
    loginScanMessage = 'Scan erfolgreich! Server wird angefragt, bitte warten!';
    checkHomeserverAction(homeserver, context);
  }

  void checkHomeserverAction(String homeserver, BuildContext context) async {
    getPIN(context);
    // if (!homeserver.startsWith('https://')) {
    //   homeserver = 'https://$homeserver';
    // }
    // final success =
    //     await checkHomeserver(homeserver, Matrix.of(context).client);

    // if (success != false) {
    //   checkPin(context);
    // } else {
    //   setState(() {
    //     loginScanImage = 'assets/networkproblem.png';
    //     loginScanMessage =
    //         'Es kann keine Verbindung mit dem Server aufgebaut werden! Überprüfe deine Internetverbindung oder versuche es später nochmal.';
    //   });
    // }
  }

  void getPIN(BuildContext context) async {
    final pin = await enterPIN();
    password = password + pin;
    login(context);
  }

  void login(BuildContext context) async {
    final matrix = Matrix.of(context);

    setState(() {
      //pr.show();
      const CircularProgressIndicator();
      usernameError = '';
      passwordError = '';
      loading = true;
      loginScanImage = 'assets/favicon.png';
      loginScanMessage = 'Deine Zugangsdaten werden geprüft... ';
    });
    try {
      await matrix.getLoginClient().login(
            LoginType.mLoginPassword,
            identifier: AuthenticationUserIdentifier(user: username),
            password: password,
            initialDeviceDisplayName: PlatformInfos.clientName,
          );
    } on MatrixException catch (exception) {
      setState(() {
        passwordError = exception.errorMessage;
        loginScanImage = 'assets/wrongpin.png';
        loginScanMessage = 'Bitte an Admin melden: $passwordError';
      });
      await _wrongPinDialog();
      return setState(() => loading = false);
    } catch (exception) {
      setState(() {
        passwordError = exception.toString();
        loginScanImage = 'assets/wrongpin.png';
        loginScanMessage = 'Bitte an Admin melden: $passwordError';
      });
      await _wrongPinDialog();
      return setState(() => loading = false);
    }

    if (mounted) setState(() => loading = false);
  }

  Timer? _coolDown;

  void checkWellKnownWithCoolDown(String userId, BuildContext context) async {
    _coolDown?.cancel();
    _coolDown = Timer(
      const Duration(seconds: 1),
      () => _checkWellKnown(userId),
    );
  }

  void _checkWellKnown(String userId) async {
    if (mounted) setState(() => usernameError = null);
    if (!userId.isValidMatrixId) return;
    final oldHomeserver = Matrix.of(context).getLoginClient().homeserver;
    try {
      var newDomain = Uri.https(userId.domain!, '');
      Matrix.of(context).getLoginClient().homeserver = newDomain;
      DiscoveryInformation? wellKnownInformation;
      try {
        wellKnownInformation =
            await Matrix.of(context).getLoginClient().getWellknown();
        if (wellKnownInformation.mHomeserver.baseUrl.toString().isNotEmpty) {
          newDomain = wellKnownInformation.mHomeserver.baseUrl;
        }
      } catch (_) {
        // do nothing, newDomain is already set to a reasonable fallback
      }
      if (newDomain != oldHomeserver) {
        await Matrix.of(context).getLoginClient().checkHomeserver(newDomain);

        if (Matrix.of(context).getLoginClient().homeserver == null) {
          Matrix.of(context).getLoginClient().homeserver = oldHomeserver;
          // okay, the server we checked does not appear to be a matrix server
          Logs().v(
            '$newDomain is not running a homeserver, asking to use $oldHomeserver',
          );
          final dialogResult = await showOkCancelAlertDialog(
            context: context,
            useRootNavigator: false,
            message:
                L10n.of(context)!.noMatrixServer(newDomain, oldHomeserver!),
            okLabel: L10n.of(context)!.ok,
            cancelLabel: L10n.of(context)!.cancel,
          );
          if (dialogResult == OkCancelResult.ok) {
            if (mounted) setState(() => usernameError = null);
          } else {
            Navigator.of(context, rootNavigator: false).pop();
            return;
          }
        }
        usernameError = null;
        if (mounted) setState(() {});
      } else {
        Matrix.of(context).getLoginClient().homeserver = oldHomeserver;
        if (mounted) {
          setState(() {});
        }
      }
    } catch (e) {
      Matrix.of(context).getLoginClient().homeserver = oldHomeserver;
      usernameError = e.toLocalizedString(context);
      if (mounted) setState(() {});
    }
  }

  Future<bool> checkHomeserver(dynamic homeserver, Client client) async {
    await client.checkHomeserver(homeserver);
    return true;
  }

  Future<String> enterPIN({
    String? titleText,
    String? confirmText,
    String? cancelText,
    String? hintText,
    String? labelText,
    String? prefixText,
    String? suffixText,
    bool password = true,
    bool multiLine = false,
  }) async {
    final textEditingController = TextEditingController();
    final controller = textEditingController;
    late String input;
    await showDialog(
      context: context,
      builder: (c) => AlertDialog(
        title: Text(titleText ?? 'PIN-Eingabe'),
        content: TextField(
          controller: controller,
          autofocus: true,
          autocorrect: false,
          onSubmitted: (s) {
            input = s;
            Navigator.of(c).pop();
          },
          minLines: multiLine ? 3 : 1,
          maxLines: multiLine ? 3 : 1,
          obscureText: password,
          textInputAction: multiLine ? TextInputAction.newline : null,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            hintText: hintText,
            labelText: labelText,
            prefixText: prefixText,
            suffixText: suffixText,
            prefixStyle: TextStyle(color: Theme.of(context).primaryColor),
            suffixStyle: TextStyle(color: Theme.of(context).primaryColor),
            border: const OutlineInputBorder(),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              cancelText?.toUpperCase() ??
                  L10n.of(context)!.cancel.toUpperCase(),
              style: const TextStyle(color: Colors.blueGrey),
            ),
            onPressed: () => Navigator.of(c).pop(),
          ),
          TextButton(
            child: Text(
              confirmText?.toUpperCase() ??
                  L10n.of(context)!.confirm.toUpperCase(),
            ),
            onPressed: () {
              input = controller.text;
              Navigator.of(c).pop();
            },
          ),
        ],
      ),
    );
    return input;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mit QR-Scan einloggen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Image.asset(loginScanImage),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 64.0,
                ),
                child: Text(
                  loginScanMessage,
                  style: const TextStyle(fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            if (loading == false)
              Hero(
                tag: 'loginwithScan',
                child: Container(
                  width: double.infinity,
                  height: 50,
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      foregroundColor: Theme.of(context).colorScheme.onPrimary,
                      padding: const EdgeInsets.symmetric(vertical: 12),
                    ),
                    onPressed: scan,
                    icon: const Icon(
                      Icons.qr_code_rounded,
                    ),
                    label: Text(
                      L10n.of(context)!.scanQrCode.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            ),
          ],
        ),
      ),
    );
  }

  Future scan() async {
    try {
      final barcode = await BarcodeScanner.scan();

      final encryptedString = barcode.rawContent;

      final decryptedString = await QrEncrypter().decrypt(encryptedString);

      credentials(decryptedString);

      setState(() {
        loginScanImage = 'assets/scansuccess.png';
        loginScanMessage = 'Scannen hat geklappt';
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          loginScanMessage =
              'Bitte erlaube zum Scannen den Zugriff auf die Kamera!';
        });
      } else {
        setState(() {
          loginScanImage = 'assets/badqr.png';
        });
        setState(() => loginScanMessage = 'Unknown error: $e');
      }
    } on FormatException {
      setState(
        () => loginScanMessage = 'NutzerIn hat den Scanvorgang abgebrochen!',
      );
    } catch (e) {
      setState(() => loginScanMessage = 'Der Scan hat nicht geklappt: $e');
    }
  }

  Future<void> _wrongPinDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('PIN stimmt nicht!'),
          content: const SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Die PIN-Nummer ist nicht richtig!'),
                Text(' '),
                Text('Versuche es nochmal oder melde dich beim Admin.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

//   Future<void> _badQrDialog() async {
//     return showDialog<void>(
//       context: context,
//       barrierDismissible: false, // user must tap button!
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: const Text('Konto nicht vorhanden!'),
//           content: const SingleChildScrollView(
//             child: ListBody(
//               children: <Widget>[
//                 Text('Das Konto wurde wahrscheinlich deaktiviert.'),
//                 Text(' '),
//                 Text('Melde dich beim Admin.'),
//               ],
//             ),
//           ),
//           actions: <Widget>[
//             TextButton(
//               child: const Text('Ok'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }
}

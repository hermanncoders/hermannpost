import 'package:fluffychat/utils/matrix_sdk_extensions/filtered_timeline_extension.dart';
import 'package:flutter/widgets.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix/matrix.dart';

import '../config/app_config.dart';
import 'date_time_extension.dart';

extension RoomStatusExtension on Room {
  CachedPresence? get directChatPresence =>
      client.presences[directChatMatrixID];

  String getLocalizedStatus(BuildContext context) {
    if (isDirectChat) {
      final directChatPresence = this.directChatPresence;
      if (directChatPresence != null &&
          (directChatPresence.lastActiveTimestamp != null ||
              directChatPresence.currentlyActive != null)) {
        if (directChatPresence.statusMsg?.isNotEmpty ?? false) {
          return directChatPresence.statusMsg!;
        }
        if (directChatPresence.currentlyActive == true) {
          return L10n.of(context)!.currentlyActive;
        }
        if (directChatPresence.lastActiveTimestamp == null) {
          return L10n.of(context)!.lastSeenLongTimeAgo;
        }
        final time = directChatPresence.lastActiveTimestamp!;
        return L10n.of(context)!
            .lastActiveAgo(time.localizedTimeShort(context));
      }
      return L10n.of(context)!.lastSeenLongTimeAgo;
    }
    return L10n.of(context)!
        .countParticipants(summary.mJoinedMemberCount.toString());
  }

  String getLocalizedTypingText(BuildContext context) {
    var typingText = '';
    final typingUsers = this.typingUsers;
    typingUsers.removeWhere((User u) => u.id == client.userID);

    if (AppConfig.hideTypingUsernames) {
      typingText = L10n.of(context)!.isTyping;
      if (typingUsers.first.id != directChatMatrixID) {
        typingText =
            L10n.of(context)!.numUsersTyping(typingUsers.length.toString());
      }
    } else if (typingUsers.length == 1) {
      typingText = L10n.of(context)!.isTyping;
      if (typingUsers.first.id != directChatMatrixID) {
        typingText =
            L10n.of(context)!.userIsTyping(typingUsers.first.calcDisplayname());
      }
    } else if (typingUsers.length == 2) {
      typingText = L10n.of(context)!.userAndUserAreTyping(
        typingUsers.first.calcDisplayname(),
        typingUsers[1].calcDisplayname(),
      );
    } else if (typingUsers.length > 2) {
      typingText = L10n.of(context)!.userAndOthersAreTyping(
        typingUsers.first.calcDisplayname(),
        (typingUsers.length - 1).toString(),
      );
    }
    return typingText;
  }

  List<Receipt> getSeenByUsers(Timeline timeline, {String? eventId}) {
    if (timeline.events.isEmpty) return [];
    final List<Event> seenEvents = [];
    for (final event in timeline.events) {
      if (!event.isState) {
        seenEvents.add(event);
      }
    }
    if (seenEvents.isNotEmpty) {
      eventId ??= seenEvents.first.eventId;
    }
    final lastReceipts = <User>{};
    final seenReceipts = <Receipt>{};
    // now we iterate the timeline events until we hit the first rendered event

    for (final event in timeline.events) {
      lastReceipts.addAll(event.receipts.map((r) => r.user));
      seenReceipts.addAll(event.receipts);
      if (event.receipts.isNotEmpty) {
        for (final receipt in event.receipts) {
          debugPrint(
            'seen by ${receipt.user.displayName}  at ${receipt.time.toString()}',
          );
        }
      }

      if (event.eventId == eventId) {
        break;
      }
    }
    lastReceipts.removeWhere(
      (user) =>
          user.id == client.userID ||
          user.id == timeline.events.first.senderId ||
          user.displayName == null,
    );
    seenReceipts.removeWhere(
      (element) =>
          element.user.id == client.userID ||
          element.user.id == timeline.events.first.senderId ||
          element.user.displayName == null,
    );
    //return lastReceipts.toList();
    final sortedReceipts = seenReceipts.toList();
    sortedReceipts.sort((item1, item2) => item1.time.compareTo(item2.time));
    return sortedReceipts;
  }
}

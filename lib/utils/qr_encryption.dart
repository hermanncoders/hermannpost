import 'package:encrypt/encrypt.dart' as enc;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class QrEncrypter {
  encrypt(nonEncryptedString) async {
    final keyutf8 = await rootBundle.loadString('assets/keys/keyaes256cbc.txt');
    final ivutf8 = await rootBundle.loadString('assets/keys/ivaes256cbc.txt');
    final key = enc.Key.fromUtf8(keyutf8);
    final iv = enc.IV.fromUtf8(ivutf8);
    final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));
    final encryptedString =
        encrypter.encrypt(nonEncryptedString, iv: iv).base64;
    return encryptedString;
  }

  decrypt(encryptedString) async {
    if (encryptedString == '') {
      return;
    }
    final keyutf8 = await rootBundle.loadString('assets/keys/keyaes256cbc.txt');
    final ivutf8 = await rootBundle.loadString('assets/keys/ivaes256cbc.txt');
    final key = enc.Key.fromUtf8(keyutf8);
    final iv = enc.IV.fromUtf8(ivutf8);
    final thisEncryptedString = enc.Encrypted.fromBase64(encryptedString);
    final encrypter = enc.Encrypter(enc.AES(key, mode: enc.AESMode.cbc));
    final decryptedString = encrypter.decrypt(thisEncryptedString, iv: iv);
    if (kDebugMode) {
      // debugPrint('Decrypted string is $decryptedString');
    }
    return decryptedString;
  }
}
